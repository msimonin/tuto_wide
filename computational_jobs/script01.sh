#!/usr/bin/env bash

function my_function() {
    echo "Calling my_function with $@"
    for i in $(seq 0 $(($RANDOM % 15 + 15)))
    do 
        echo "foo-$i"
        sleep 1
    done
    echo "Called my_function with $@"
}

my_function $@
