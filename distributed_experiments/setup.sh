#!/usr/bin/env bash

virtualenv -p python3 venv
source venv/bin/activate

pip install -r requirements.txt

# Enable this env to jupyter
python -m ipykernel install --user --name g5k_quickoverview --display-name "Python3 (g5k_quickoverview)"
