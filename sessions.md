# XUG: Grid'5000 tutorial (3 sessions) [NOTES]

Table Of Content

- [Session 1](#session-1)
    - [who’s there](#who’s-there-)
    - [Presentation G5k](#presentation-g5k)
    - [What for ?](#what-for-)
    - [Getting help](#getting-help)
    - [First Hands on](#hands-on)
    - [Additional tricks](#additional-tricks)
        - [Passive mode](#passive-mode)
        - [Besteffort jobs](#besteffort-jobs)
        - [Scanscript](#scanscript)
    - [SPMD jobs](#spmd-jobs)
       - [Using jobs arrays](#using-job-arrays)
       - [Using GNU parallel](#using-gnu-parallel)
    - [Software dependencies management](#software-dependencies-management)
    - [Using more than one node](#using-more-than-one-node)
    - [Quotas](#quotas)
    - 
- [Session 2](#session-2)
    - [Execo](#execo)
        - [ParamSweeper](#paramsweeper)
        - [Infrastructure as a parameter](#infrastructure-as-a-parameter)
        - [Combining the two :)](#combining-the-two-)
    - [Grid5000 REST API](#grid’5000-rest-api)
        - [Get Started](#get-started)
        - [Python-grid5000](#python-grid5000)

# Session 1

## Who's there ?

TODO

## Presentation G5k

![](https://notes.inria.fr/uploads/upload_a7335cb6ab9f7e223997cbc12040af9d.png)


Hardware: https://www.grid5000.fr/w/Hardware


## What for ?

- **Computational jobs**
 _You are interested in running a function a get the result (as fast as possible)_
 
    - MPI jobs
    - Machine Learning train/test
    - Simulations
    - ...

=> (Roughly) You need to pick the right set of resources
(#CPU cores / amount of mem / accelerators) to run your computation.

session 1 & 2: covers these aspects

- **Experiment with custom distributed system**
  _You have a system  and want to make vary the environment where it is running, measure stuffs, study the behavior against a given workload_

  => Roughly: it's more than picking the right set of resources

    - *system* == anything above the OS (OS included)
    - *vary the environment* == reconfigure OS / underlying compute (bare-metal / virtualisation  ...)  / network condition (e.g mimic geo-distribution ... )
    - *measure stuffs* == instrument (yourself) the deployment to get metrics 

    Grid'5000 provides some facilities to enable the above (session 2&3) + some specific client library.


## Getting Help

- Community tools:
    - Documentation: https://www.grid5000.fr/
    - FAQ: https://www.grid5000.fr/w/FAQ
    - Mailing lists:
        - users@lists.grid5000.fr [500 subscribers - general questions usage/practices..]
        - support-staff@lists.grid5000.fr [a dozen of subscribers / admins - bug reports]
    - Mattermost: https://mattermost.inria.fr/grid5000

- Local help:
    - AskTheSed: https://sed-rennes.gitlabpages.inria.fr/ask-the-sed/

## First Hands-on

Initial Glossary:
- `nodes/server`: compute resources, you can reserve a slice of it (e.g. 1 core) to run a computation
- `cluster`: homogeneous set of nodes (on some cluster there might be some sligh differences: [chifflot](https://www.grid5000.fr/w/Lille:Hardware#chifflot), )
- `site`: set of cluster located in the same location (room) somewhere in France
- `scheduler`: program that decides on the placement of the user program on nodes. Grid'5000 uses [OAR](http://oar.imag.fr/docs/latest/) (other uses SLURM, SGE ...).
    - There's one scheduler per site
- `job`: unit of scheduling:
    ![](https://notes.inria.fr/uploads/upload_3d6ba65ffec2c3c6f46462d0121981c3.png)
    A job has a lot of parameter that can influence its placement on the platform.

- `frontend`: Machine from which you can interact with a scheduler. 
    - There's as many frontends as sites.

> Notes:
> - you can perform the above using a regular SSH connection to any site frontend
> - alternatively you can use a terminal from the jupyter lab interface: https://intranet.grid5000.fr/notebooks


:::info
⏳ **DO: https://www.grid5000.fr/w/Getting_Started**
    - for now only part 1., 2., 3., 4. 
:::

*Some useful (control) commands:*

- `oarsub`: submit jobs
- `oarstat`: check job status
        - `oarstat -u [-f]`:  show your present and future jobs. Optionaly add more information
        - `oarstat -j <jobid> [-f]`: show the status of a specific job. Optionaly add more information 
        - `oarstat -j 123456 --json`: same but with a json output. Can be piped to `jq`: ` oarstat -j 123456 --json | jq '.[].command'`
- `oardel`: delete a job

*Discover free resources:*
- DrawGantt: https://intranet.grid5000.fr/oar/Rennes/drawgantt-svg/
- Funk command: `funk`

## Additional tricks

> Mostly based on https://www.grid5000.fr/w/Advanced_OAR#Connection_to_the_job.27s_nodes
> but with some personal sauce :)

Let's consider the following function saved in `my_function.sh`.

```bash
#!/usr/bin/env bash

function my_function() {
    echo "Calling my_function with $@"
    for i in $(seq 0 $(($RANDOM % 15 + 15)))
    do 
        echo "foo-$i"
        sleep 1
    done
    echo "Called my_function with $@"
}

my_function $@
```


### Passive mode 

✏✏✏

1. Create the file `my_function.sh`
2. Create a passive job that launches `my_function.sh p1 p2 p3 p4` on start.
3. Check the statues and the stdour/stderr files (should be named `OAR.<jobid>.stdout`/`OAR.<jobid>.stderr`)

✏✏✏

### Besteffort jobs

- Besteffort jobs are low priority jobs: can be killed by the scheduler to favour higher priority jobs.
- **Besteffort jobs do not consume your quota**

Let's check this:


✏✏✏

1. Create a besteffort job: `oarsub -t besteffort "sleep 3600"`. Wait for it to start
2. Check the status of your jobs
3. Create a (regular) job that targets the node reserved by the besteffor job : `oarsub -I -l "{host in ('parasilo-12.rennes.grid5000.fr')}"` (adapt).
4. Check the status

✏✏✏


:::info
⏳ What happen if you start a `besteffort + idempotent` job ?
:::

### Scanscript

OAR can scan the beginning of the passed script to build the option of `oarsub`.
**Pro: your script is self-contained**

Example:

```bash
#OAR -l cores=1,walltime=00:10:00
#OAR -O /home/msimonin/logs/%jobid%.stdout
#OAR -E /home/msimonin/logs/%jobid%.stderr

#!/usr/bin/env bash

function my_function() {
    echo "Calling my_function with $@"
    for i in $(seq 0 $(($RANDOM % 15 + 15)))
    do 
        echo "foo-$i"
        sleep 1
    done
    echo "Called my_function with $@"
}

my_function $@
```

Launch it:
```bash
frontend)mkdir -p /home/msimonin/logs
frontend)oarsub -S ./my_function.sh p1 p2 p3
```

## SPMD jobs

In the following we want to run `my_function.sh` with 4 parameters p1, p2, p3 and p4, with:

- $p1\in\left\{p11, p12, p13\right\}$
- $p2\in\left\{p21, p22, p23\right\}$
- $p3\in\left\{p31, p32, p33\right\}$
- $p4\in\left\{p41, p42, p43\right\}$

The purpose of the above is to simulate an exhaustive search over the parameter space (e.g. we want to search the parameters that minimize a function).
We want to launch as many processes ($3^4=81$) as combination of parameters to explore using each a single core.

### Using jobs arrays

OAR suports natively this use case using `job array`.

:::info
⏳ Launch all the jobs in a single `oarsub` command (check `oarsub` man page).
:::


### Using GNU parallel

:::info
⏳ Go through: https://www.grid5000.fr/w/GNU_Parallel
:::


## Software dependencies management

:::info
⏳ https://www.grid5000.fr/w/Getting_Started#Example_usage
:::

## Using more than one node

✏✏✏
1. Create a job with two hosts in interactive mode
2. On one host start an iperf server: `iperf -s`
3. On the other host starts an iperf client. `iperf -c <server addr>`
4. Check the throughput

✏✏✏

:::info
⏳ Automate the above: write a script that can run in a passive mode and that does the above. Enjoy ;)
:::

need to CP this:

```bash
#!/usr/bin/env bash

# resource selection
#OAR -l nodes=2/core=1,walltime=00:10:00

# alternative two clusters with one core each
# -l  {cluster='parasilo'}/core=1+{cluster='paravance'}/core=1,walltime=00:10:00

set -x

other=$(cat $OAR_FILE_NODES | grep -v $(hostname) | uniq)

# start the server on the first node in background
iperf -s  &

# wait a bit...
sleep 3

# start iperf client on the other node by connecting back to the server
oarsh $other "iperf -c $(hostname)"
```

## Quotas

:::info
⏳ https://www.grid5000.fr/w/Grid5000:UsagePolicy
:::

Rule of thumb:
- daytime, week day: a whole cluster for 2 hours  maximum
- nightime: no restriction (but need to reserve in advance more likely)
- production queues have different rules
- These are soft limits...



# Session 2

Rationale: some Limitations of OAR:
- Some limitations on the number of running/pending jobs
    - 200 on G5k
    - 1000+ on Igrida
- hard to make the infrastructure a parameter of the experiment.
    - e.g. you want to run the iperf example on all the pair of possible clusters (or machines...).
    - e.g you want to run the iperf example on all pair of possible nodes that have 2 network interfaces put on a dedicated VLAN

To achieve the above, it's better to use higher level tools (not only bash+OAR)

## Execo

:::info
⏳ https://mimbert.gitlabpages.inria.fr/execo/index.html
:::

Main features:
- Can do reservation on a OAR testbed (G5K  / Igrida)
- Can manage remote processes (a lot)
- Can ease parameter exploration (ParamSweeper)

Install the library
```
$frontend) python3 -m pip install --user -U execo 
```
### ParamSweeper

Purpose: keep track of what parameters has been processed, what parameters needs to be restarted ...

:::info
⏳https://mimbert.gitlabpages.inria.fr/execo/execo_engine.html?highlight=paramsweeper#execo_engine.sweep.ParamSweeper
:::

Example:

```python
# file: sweep_example.py
from pathlib import Path

from execo_engine import ParamSweeper, sweep

def do_stuff(parameter):
    """A dummy benchmark function"""
    print(parameter)
   

def do_stuff_but_fails_sometime(parameter, rate=0.5):
    import random
    if random.uniform(0, 1)<=rate:
        raise Exception(f"[KO] {parameter}")
    print(f"[OK] {parameter}")
 

parameters = dict(
    p=[1, 2, 4, 8, 16],
    q=[None, "0ms", "10ms", "50ms"]
)

# Cartesian product of the parameters
sweeps = sweep(parameters)

# Build the sweeper
sweeper = ParamSweeper(
    persistence_dir=str(Path("sweeps")), sweeps=sweeps, save_sweeps=True
)
parameter = sweeper.get_next()

while parameter:
    try:
        do_stuff_but_fails_sometime(parameter)
        sweeper.done(parameter)
    except Exception as e:
        sweeper.skip(parameter)
    finally:
        parameter = sweeper.get_next()
```
- When `do_stuff` is used: a single run is needed to cover all parameters
- When `do_stuff_but_fails_sometime` is used: run $n$ will restart faulty parameters of run $n-1$.

Note that everything is stored under the `persistence_dir`, you can remove it to start from scratch.

- Launch it:
```
frontend) python3 sweep_example.py
```

### Infrastructure as parameter

:::info
from
⏳ https://mimbert.gitlabpages.inria.fr/execo/userguide.html#oarsub-example
but with some updates ...
:::

#### First trial

```python
# file: test_execo.py
from execo import *
from execo_g5k import *
import itertools


conn_params = default_oarsh_oarcp_params.copy()

site = "rennes"
[(jobid, site)] = oarsub([
  ( OarSubmission(resources = "/cluster=2/nodes=2"), site)
])

if jobid:
    try:
        nodes = []
        wait_oar_job_start(jobid, site)
        nodes = get_oar_job_nodes(jobid, site)
        # group nodes by cluster
        print(nodes)
        sources, targets = [ list(n) for c, n in itertools.groupby(
          sorted(nodes, key=get_host_cluster),  get_host_cluster) ]
        print(f"sources = {sources}")
        print(f"targets = {targets}")
        servers = Remote("iperf -s",
                         targets,
                         connection_params = conn_params)
        clients = Remote("iperf -c {{[t.address for t in targets]}}",
                         sources,
                         connection_params = conn_params)
        print("starting servers")
        with servers.start():
            print("starting clients")
            sleep(1)
            clients.run()
        clients.wait()
        print(Report([ servers, clients ]).to_string())
        for index, p in enumerate(clients.processes):
            print("client %s -> server %s - stdout:" % (p.host.address,
                                                        targets[index].address))
            print(p.stdout)
    finally:
        oardel([(jobid, site)])
```

Launch it:
```
frontend) OAR_JOB_KEY_FILE=~/.ssh/id_rsa python3 test_execo.py
```

#### Make the infratructure a parameter

```python
# file: test_execo.py
from execo import *
from execo_g5k import *
import itertools


conn_params = default_oarsh_oarcp_params.copy()

def bench(cluster1, cluster2, site, nodes=2):
    resources = "{cluster='%s'}/nodes=%s+{cluster='%s'}/nodes=%s" % (cluster1, nodes, cluster2, nodes)
    print(resources)
    [(jobid, site)] = oarsub([
      ( OarSubmission(resources = resources), site)
    ])

    if jobid:
        try:
            nodes = []
            wait_oar_job_start(jobid, site)
            nodes = get_oar_job_nodes(jobid, site)
            # group nodes by cluster
            print(nodes)
            sources, targets = [ list(n) for c, n in itertools.groupby(
              sorted(nodes, key=get_host_cluster),  get_host_cluster) ]
            print(f"sources = {sources}")
            print(f"targets = {targets}")
            servers = Remote("iperf -s",
                             targets,
                             connection_params = conn_params)
            clients = Remote("iperf -c {{[t.address for t in targets]}}",
                             sources,
                             connection_params = conn_params)
            print("starting servers")
            with servers.start():
                print("starting clients")
                sleep(1)
                clients.run()
            clients.wait()
            print(Report([ servers, clients ]).to_string())
            for index, p in enumerate(clients.processes):
                print("client %s -> server %s - stdout:" % (p.host.address,
                                                            targets[index].address))
                print(p.stdout)
        finally:
            oardel([(jobid, site)])
            
if __name__ == "__main__":
    bench("parasilo", "paravance", "rennes")
    # Reserving on another site should be also possible
    # bench("grving", "gros", "nancy")
```

Launch it:
```
frontend) OAR_JOB_KEY_FILE=~/.ssh/id_rsa python3 test_execo.py
```

### Combining the two :)

```python
import itertools
from pathlib import Path

from execo import *
from execo_g5k import *
from execo_engine import ParamSweeper, sweep

conn_params = default_oarsh_oarcp_params.copy()

def bench(cluster1, cluster2, site, nodes=2):
    resources = "{cluster='%s'}/nodes=%s+{cluster='%s'}/nodes=%s" % (cluster1, nodes, cluster2, nodes)
    print(resources)
    [(jobid, site)] = oarsub([
      ( OarSubmission(resources = resources), site)
    ])

    if jobid:
        try:
            nodes = []
            wait_oar_job_start(jobid, site)
            nodes = get_oar_job_nodes(jobid, site)
            # group nodes by cluster
            print(nodes)
            sources, targets = [ list(n) for c, n in itertools.groupby(
              sorted(nodes, key=get_host_cluster),  get_host_cluster) ]
            print(f"sources = {sources}")
            print(f"targets = {targets}")
            servers = Remote("iperf -s",
                             targets,
                             connection_params = conn_params)
            clients = Remote("iperf -c {{[t.address for t in targets]}}",
                             sources,
                             connection_params = conn_params)
            print("starting servers")
            with servers.start():
                print("starting clients")
                sleep(1)
                clients.run()
            clients.wait()
            print(Report([ servers, clients ]).to_string())
            for index, p in enumerate(clients.processes):
                print("client %s -> server %s - stdout:" % (p.host.address,
                                                            targets[index].address))
                print(p.stdout)
        except Exception as e:
            # Let it fail badly so that we are aware of the problem at
            # the sweep level :)
            raise e
        finally:
            # clean anyway
            oardel([(jobid, site)])
            
if __name__ == "__main__":
    # build all the wanted parameters
    sweeps = [
            ("paravance", "parasilo", "rennes"),
            ("gros", "grisou", "nancy"),
            ("nova", "hercule", "lyon"),
    ]

    # Build the sweeper
    sweeper = ParamSweeper(
        persistence_dir=str(Path("sweeps")), sweeps=sweeps, save_sweeps=True
    )
    parameter = sweeper.get_next()

    while parameter:
        print(parameter)
        try:
            bench(*parameter)
            sweeper.done(parameter)
        except Exception as e:
            raise e
            sweeper.skip(parameter)
        finally:
            parameter = sweeper.get_next()
```

Launch it:
```
frontend) OAR_JOB_KEY_FILE=~/.ssh/id_rsa python3 test_execo.py
```

### Going further

- Check out the documentation: https://mimbert.gitlabpages.inria.fr/execo/
    - Learn about `Engine` (simplify writing above codes)
    - If you have plenty of jobs, you can minionize them :) (https://pypi.org/project/minionize/)

- But you best bet is to discuss with us, for instance: https://sed-rennes.gitlabpages.inria.fr/ask-the-sed/

## Grid'5000 REST API

In the above we used:
```python
sweeps = [
        ("paravance", "parasilo", "rennes"),
        ("gros", "grisou", "nancy"),
        ("nova", "hercule", "lyon"),
]
```
- Question: how to get the list of available clusters/sites ?
- Answer: The REST API !     

## Get started


:::info
⏳ https://api.grid5000.fr/doc/3.0/
:::

Examples (using bash/curl):
```bash
frontend) curl https://api.grid5000.fr/3.0/sites

# piped into jq to be more readable
frontend) curl https://api.grid5000.fr/3.0/sites | jq .

# get only the uid of all return uid
frontend) curl https://api.grid5000.fr/3.0/sites | jq .items[].uid
```

You can do that from your laptop (you need to pass your credentials)

```
$laptop) curl -u msimonin https://api.grid5000.fr/3.0/sites
```
## Python-grid5000

:::info
⏳ https://msimonin.gitlabpages.inria.fr/python-grid5000/
:::

- Go through the examples. 
- Configure and test some of them (from your laptop for instance :) )
