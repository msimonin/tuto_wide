#!/usr/bin/env bash

set -x

virtualenv -p python3 venv
source venv/bin/activate

python3 -m pip install -U pip
python3 -m pip install -r requirements.txt

# Enable this env to jupyter
python3 -m ipykernel install --user --name g5k_quickoverview --display-name "Python3 (g5k_quickoverview)"
